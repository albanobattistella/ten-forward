Ten Forward
===========

Configure port forwarding on your gateway through the NAT-PMP protocol.

It allows you to create multiple mapping configurations, which can individually be requested.
This tool will allow you to run a local game server (minecraft LAN, Mindustry, Stardew valley., etc..) and make it accessible online.
It is tested with the NAT-PMP enabled ProtonVPN, and should be compatible with any other NAT-PMP capable gateway.
