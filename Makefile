run:
	RUST_LOG='trace' G_MESSAGES_DEBUG=all mold -run cargo r
run-debug:
	GTK_DEBUG=interactive  G_MESSAGES_DEBUG=all RUST_LOG='trace' mold -run cargo r

icons:
	rsvg-convert -w 16 -h 16  assets/icon.svg -o assets/icon16.png
	rsvg-convert -w 32 -h 32  assets/icon.svg -o assets/icon32.png
	rsvg-convert -w 64 -h 64  assets/icon.svg -o assets/icon64.png
	rsvg-convert -w 128 -h 128  assets/icon.svg -o assets/icon128.png
	rsvg-convert -w 256 -h 256  assets/icon.svg -o assets/icon256.png
	rsvg-convert -w 512 -h 512  assets/icon.svg -o assets/icon512.png
	rsvg-convert -w 1024 -h 1024  assets/icon.svg -o assets/icon1024.png
