use std::{cell::Cell, net::Ipv4Addr};

use adw::prelude::*;
use i18n_embed_fl::fl;
use relm4::{prelude::*, ComponentParts, ComponentSender, SimpleComponent};
use tracing::{debug, error, info};

use crate::{ToastMessage, LOCALE};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Message {
    Set(Ipv4Addr),
    SetShouldSave(bool),
    PublicIp(Option<Ipv4Addr>),
    CancelEditing,
    Edit,
}

impl Message {
    pub fn is_set(&self) -> bool {
        matches!(self, Message::Set(_))
    }
}

#[derive(Debug, Clone, Copy)]
pub enum PublicIp {
    Loading,
    Error,
    Some(Ipv4Addr),
}

impl PublicIp {
    fn activatable(&self) -> bool {
        matches!(self, Self::Some(_))
    }

    fn tooltip_text(&self) -> Option<String> {
        matches!(self, Self::Some(_)).then_some(fl!(LOCALE, "copy-to-clipboard"))
    }

    fn subtitle(&self) -> String {
        match self {
            Self::Some(ip) => format!("{ip}"),
            Self::Error => fl!(LOCALE, "error"),
            _ => String::new(),
        }
    }

    fn css_classes(&self) -> &'static [&'static str] {
        match self {
            Self::Error => &["error", "property"],
            Self::Some(_) => &["property"],
            Self::Loading => &[],
        }
    }
}

const DEFAULT_IP: Ipv4Addr = Ipv4Addr::new(192, 168, 0, 1);

#[derive(Debug, Clone)]
pub struct Gateway {
    pub value: Ipv4Addr,
    pub public_ip: PublicIp,
    editing: bool,
    pub should_save_gateway: bool,
    public_ip_spinner: gtk::Spinner,
    spinner_present: Cell<bool>,
    icon: gtk::Image,
    icon_present: Cell<bool>,
}

impl Gateway {
    fn add_spinner(&self) -> Option<&gtk::Spinner> {
        if !self.spinner_present.get() && matches!(self.public_ip, PublicIp::Loading) {
            self.spinner_present.set(true);
            Some(&self.public_ip_spinner)
        } else {
            None
        }
    }
    fn remove_spinner(&self) -> Option<&gtk::Spinner> {
        if self.spinner_present.get() && !matches!(self.public_ip, PublicIp::Loading) {
            self.spinner_present.set(false);
            Some(&self.public_ip_spinner)
        } else {
            None
        }
    }
    fn add_icon(&self) -> Option<&gtk::Image> {
        if !self.icon_present.get() && matches!(self.public_ip, PublicIp::Some(_)) {
            self.icon_present.set(true);
            Some(&self.icon)
        } else {
            None
        }
    }
    fn remove_icon(&self) -> Option<&gtk::Image> {
        if self.icon_present.get() && !matches!(self.public_ip, PublicIp::Some(_)) {
            self.icon_present.set(false);
            Some(&self.icon)
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum OutMessage {
    Save,
    GatewayChanged(Ipv4Addr),
    Toast(ToastMessage),
}

#[relm4::component(pub)]
impl SimpleComponent for Gateway {
    type Init = Option<Ipv4Addr>;
    type Input = Message;
    type Output = OutMessage;

    view! {
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_halign: gtk::Align::Center,

            adw::Clamp {
                set_orientation: gtk::Orientation::Vertical,
                set_maximum_size: 700,
                set_tightening_threshold: 700,
                set_hexpand: true,

                #[transition = "SlideLeftRight"]
                if !model.editing {
                    gtk::Box {
                        set_margin_all: 10,
                        set_spacing: 10,
                        set_orientation: gtk::Orientation::Vertical,
                        gtk::Button {
                            set_hexpand: true,
                            set_label: &fl!(LOCALE, "configure"),
                            connect_clicked[sender] => move |_| {
                                sender.input(Message::Edit)
                            }
                        },
                        gtk::ListBox {
                            add_css_class: "boxed-list",
                            set_hexpand: true,

                            adw::ActionRow {
                                set_title: &fl!(LOCALE, "gateway"),
                                add_css_class: "property",
                                set_hexpand: true,
                                #[watch]
                                set_subtitle: &format!("{}", model.value),
                                set_tooltip_text: Some(&fl!(LOCALE, "copy-to-clipboard")),
                                add_suffix: &gtk::Image::from_icon_name("edit-copy-symbolic"),
                                set_activatable: true,
                                connect_activated[sender] => move |this| {
                                    let subtitle = this.subtitle().unwrap();
                                    this.clipboard().set_text(&subtitle);
                                    sender.output(OutMessage::Toast(ToastMessage::new(fl!(LOCALE, "copied", value = subtitle.to_string())))).ok();
                                }
                            },
                            adw::ActionRow {
                                set_title: &fl!(LOCALE, "public-ip"),
                                set_hexpand: true,
                                #[watch]
                                set_subtitle: &model.public_ip.subtitle(),
                                #[watch]
                                add_suffix?: model.add_spinner(),
                                #[watch]
                                add_suffix?: model.add_icon(),
                                #[watch]
                                remove?: model.remove_spinner(),
                                #[watch]
                                remove?: model.remove_icon(),
                                #[watch]
                                set_tooltip_text: model.public_ip.tooltip_text().as_deref(),
                                #[watch]
                                set_activatable: model.public_ip.activatable(),
                                connect_activated[sender] => move |this| {
                                    let subtitle = this.subtitle().unwrap();
                                    this.clipboard().set_text(&subtitle);
                                    sender.output(OutMessage::Toast(ToastMessage::new(fl!(LOCALE, "copied", value = subtitle.to_string())))).ok();
                                },
                                #[watch]
                                set_css_classes: model.public_ip.css_classes(),
                            },
                        }
                    }
                } else {
                    gtk::Box {
                        set_orientation: gtk::Orientation::Vertical,
                        set_hexpand: true,
                        set_spacing: 10,
                        set_margin_all: 10,

                        gtk::Box {
                            add_css_class: "linked",
                            set_orientation: gtk::Orientation::Horizontal,
                            #[name = "editing_cancel"]
                            gtk::Button {
                                set_hexpand: true,
                                set_label: &fl!(LOCALE, "cancel"),
                                connect_clicked[sender] => move |_| {
                                    info!("Cancelling edition");
                                    sender.input(Message::CancelEditing)
                                },
                            },
                            #[name = "editing_commit"]
                            gtk::Button {
                                set_hexpand: true,
                                add_css_class: "suggested-action",
                                set_label: &fl!(LOCALE, "save"),
                                connect_clicked[gateway_entry, sender] => move |_| {
                                    if let Ok(ip) = gateway_entry.text().parse::<Ipv4Addr>() {
                                        sender.input(Message::Set(ip));
                                    } else {
                                        error!("Failed to parse gateway IP: {}", gateway_entry.text());
                                        sender.output(OutMessage::Toast(ToastMessage::new(fl!(LOCALE, "gateway-ip-address-must-be-an-ipv4-address")))).ok();
                                    }
                                },
                            }
                        },
                        adw::PreferencesGroup {
                            adw::SwitchRow {
                                set_title: &fl!(LOCALE, "override-default"),
                                set_tooltip_text: Some(&fl!(LOCALE, "should-this-value-become-the-default-after-a-restart")),
                                set_active: model.should_save_gateway,
                                connect_active_notify[sender] => move |b| {
                                    sender.input(Message::SetShouldSave(b.is_active()));
                                },
                            },
                            #[name = "gateway_entry"]
                            adw::EntryRow {
                                set_title: &fl!(LOCALE, "gateway-ip-address"),
                                set_text: &format!("{}", model.value),
                            },
                        },
                    }

                }
            }
        }
    }

    // Initialize the UI.
    fn init(
        state: Option<Ipv4Addr>,
        root: Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let model = Self {
            value: state.unwrap_or(DEFAULT_IP),
            public_ip: PublicIp::Loading,
            editing: false,
            should_save_gateway: state.is_some(),
            public_ip_spinner: gtk::Spinner::builder().spinning(true).build(),
            spinner_present: Cell::new(false),
            icon: gtk::Image::from_icon_name("edit-copy-symbolic"),
            icon_present: Cell::new(false),
        };
        let widgets = view_output!();
        ComponentParts { model, widgets }
    }

    fn update(&mut self, msg: Self::Input, sender: ComponentSender<Self>) {
        debug!("Updating gateway: {msg:?}");
        match msg {
            Message::SetShouldSave(should_save_gateway) => {
                self.should_save_gateway = should_save_gateway
            }
            Message::Set(ip) => {
                self.value = ip;
                self.editing = false;
                self.public_ip = PublicIp::Loading;
                sender
                    .output(OutMessage::Save)
                    .map_err(|err| {
                        error!("Failed to send out message: {err:?}");
                    })
                    .ok();
                sender
                    .output(OutMessage::GatewayChanged(ip))
                    .map_err(|err| {
                        error!("Failed to send out message: {err:?}");
                    })
                    .ok();
            }
            Message::Edit => {
                self.editing = true;
            }
            Message::PublicIp(Some(ip)) => {
                self.public_ip = PublicIp::Some(ip);
            }
            Message::PublicIp(None) => {
                debug_assert!(matches!(self.public_ip, PublicIp::Loading));
                sender
                    .output(OutMessage::Toast(ToastMessage::new(fl!(
                        LOCALE,
                        "gateway-doesnt-support-nat-pmp"
                    ))))
                    .ok();
                self.public_ip = PublicIp::Error;
            }
            Message::CancelEditing => {
                self.editing = false;
            }
        }
    }
}
