use adw::prelude::*;
use indexmap::IndexMap;
use once_cell::sync::Lazy;
use relm4::{
    factory::FactoryVecDeque, prelude::*, ComponentParts, ComponentSender, FactorySender, RelmApp,
    SimpleComponent,
};

pub const APP_ID: &str = "fr.sgued.ten_forward";
pub const APP_NAME: &str = "Ten Forward";

pub mod backend;
pub mod gateway_input;
pub mod mapping;
mod new_mapping;
pub mod state;

use backend::run_backend;
use gateway_input::{Gateway, Message as GatewayMessage, OutMessage as GatewayOut, PublicIp};
use mapping::{MappingInit, MappingModel, OutMessage as MappingOutMessage};
use new_mapping::{NewMapping, OutMessage as NewMappingMessage};

use i18n_embed::{
    fluent::{fluent_language_loader, FluentLanguageLoader},
    DesktopLanguageRequester, LanguageLoader,
};
use i18n_embed_fl::fl;
use rust_embed::RustEmbed;
use state::State;
use tokio::sync::mpsc::{unbounded_channel, UnboundedSender};
use uuid::Uuid;

/// We can't send raw `adw::Toast` because they are not `Send`. Hence we use a custom builder
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ToastMessage {
    timeout: u32,
    button_label: String,
    priority: adw::ToastPriority,
    title: String,
}

impl ToastMessage {
    pub fn new(title: String) -> Self {
        Self {
            timeout: 5,
            button_label: String::new(),
            priority: adw::ToastPriority::Normal,
            title,
        }
    }

    pub fn button_label(self, button_label: String) -> Self {
        Self {
            button_label,
            ..self
        }
    }
    pub fn priority(self, priority: adw::ToastPriority) -> Self {
        Self { priority, ..self }
    }
    pub fn title(self, title: String) -> Self {
        Self { title, ..self }
    }

    pub fn build(self) -> adw::Toast {
        adw::Toast::builder()
            .button_label(self.button_label)
            .priority(self.priority)
            .timeout(self.timeout)
            .title(self.title)
            .build()
    }
}

#[derive(Debug, Clone)]
enum Message {
    About,
    GoToGateway,
    GoToNew,
    CopyPort(u16),
    SelectedMapping(usize),
    Gateway(GatewayMessage),
    GatewayError(String),
    Mapping(Uuid, mapping::Message),
    #[allow(dead_code)]
    SaveError(String),
    NewMapping(state::Mapping),
    DeleteMapping(DynamicIndex),
    UpdateName(String, DynamicIndex),
    UpdateState(mapping::State, DynamicIndex),
    Save,
    AddToast(ToastMessage),
    None,
}

impl From<GatewayMessage> for Message {
    fn from(value: GatewayMessage) -> Self {
        Self::Gateway(value)
    }
}

#[derive(Clone, Default, Debug)]
struct MappingName {
    uuid: Uuid,
    name: String,
    state: mapping::State,
}

#[derive(Clone, Debug)]
enum MappingNameInput {
    ChangeName(String),
    ChangeState(mapping::State),
}

struct AppModel {
    mappings_name: FactoryVecDeque<MappingName>, // mappings: Mappings,
    gateway: Controller<Gateway>,
    new_mapping: Controller<NewMapping>,
    mapping_widgets: IndexMap<Uuid, Controller<MappingModel>>,
    backend: UnboundedSender<backend::Message>,
    stack: gtk::Stack,
    selector: gtk::ListBox,
    toast_overlay: adw::ToastOverlay,
    page_title: adw::WindowTitle,
    window: adw::Window,
}

impl AppModel {
    fn save(&self) -> State {
        let gateway_model = self.gateway.model();
        State {
            gateway: gateway_model
                .should_save_gateway
                .then_some(gateway_model.value),
            mappings: self
                .mapping_widgets
                .iter()
                .map(|(uuid, c)| {
                    let model = c.model();
                    let name = model.name.clone();
                    state::Mapping {
                        name,
                        uuid: *uuid,
                        ports: model.port,
                    }
                })
                .collect(),
        }
    }
}

fn mapping_out_handler(
    backend: UnboundedSender<backend::Message>,
    uuid: Uuid,
    ports: state::PortMapping,
    name_idx: DynamicIndex,
) -> impl Fn(MappingOutMessage) -> Message {
    move |i| match i {
        MappingOutMessage::Save => Message::Save,
        MappingOutMessage::Delete => Message::DeleteMapping(name_idx.clone()),
        MappingOutMessage::ChangeName(name) => Message::UpdateName(name, name_idx.clone()),
        MappingOutMessage::UpdateState(state) => Message::UpdateState(state, name_idx.clone()),
        MappingOutMessage::Start => {
            backend
                .send(crate::backend::Message::AddMapping {
                    uuid,
                    private_port: ports.private_port,
                    public_port: ports.public_port,
                    ty: ports.ty,
                })
                .map_err(|_err| tracing::error!("Failed to send backend message"))
                .ok();
            Message::UpdateState(mapping::State::Starting, name_idx.clone())
        }
        MappingOutMessage::Stop => {
            backend
                .send(crate::backend::Message::RemoveMapping(uuid))
                .map_err(|_err| tracing::error!("Failed to send backend message"))
                .ok();
            Message::UpdateState(mapping::State::Stopping, name_idx.clone())
        }
        MappingOutMessage::Copy(port) => Message::CopyPort(port),
        MappingOutMessage::Toast(toast) => Message::AddToast(toast),
    }
}

#[relm4::factory]
impl FactoryComponent for MappingName {
    type Init = MappingName;
    type Input = MappingNameInput;
    type Output = ();
    type CommandOutput = ();
    type Widgets = MappingWidgets;
    type ParentWidget = gtk::ListBox;

    view! {
        root = gtk::Box {
            set_orientation: gtk::Orientation::Horizontal,
            set_spacing: 5,
            gtk::Label {
                #[watch]
                set_label: &self.name,
            },
            gtk::Box {
                set_hexpand: true,
            },
            #[transition = "Crossfade"]
            match self.state {
                mapping::State::Idle => {
                    gtk::Image {
                        set_icon_name: Some("network-idle"),
                    }
                },
                mapping::State::Starting => {
                    gtk::Image {
                        set_icon_name: Some("network-receive-symbolic"),
                    }
                },
                mapping::State::Stopping => {
                    gtk::Image {
                        set_icon_name: Some("network-transmit-symbolic"),
                    }
                },
                mapping::State::Running(_) => {
                    gtk::Image {
                        set_icon_name: Some("network-transmit-receive-symbolic"),
                    }
                },
                mapping::State::Error => {
                    gtk::Image {
                        set_icon_name: Some("network-offline-symbolic"),
                    }
                }
            },
        }
    }

    fn update(&mut self, msg: MappingNameInput, _sender: FactorySender<Self>) {
        match msg {
            MappingNameInput::ChangeName(name) => self.name = name,
            MappingNameInput::ChangeState(state) => self.state = state,
        }
    }

    fn init_model(name: Self, _index: &DynamicIndex, _sender: FactorySender<Self>) -> Self {
        name
    }
}

#[allow(deprecated)]
#[relm4::component]
impl SimpleComponent for AppModel {
    type Init = State;

    type Input = Message;
    type Output = ();

    view! {
      #[name = "window"]
      adw::Window {
            set_title: Some(APP_NAME),
            set_default_height: 350,
            set_default_width: 550,

            #[name = "leaflet"]
            adw::Leaflet {
                set_can_navigate_back: true,

                gtk::Box {
                    set_orientation: gtk::Orientation::Vertical,
                    set_vexpand: true,

                    #[name = "sidebar_header"]
                    adw::HeaderBar {
                        #[wrap(Some)]
                        set_title_widget = &adw::WindowTitle {
                            set_title: "Ten Forward",
                        },
                        set_show_end_title_buttons: false,
                        pack_end = &gtk::MenuButton {
                            set_icon_name: "open-menu-symbolic",
                            #[wrap(Some)]
                            set_popover = &gtk::Popover {
                                gtk::Box {
                                    set_orientation: gtk::Orientation::Horizontal,
                                    gtk::Button {
                                        set_label: &fl!(LOCALE, "about-ten-forward"),
                                        set_css_classes: &["flat", "tex-button"],
                                        connect_clicked => Message::About,
                                    }
                                }
                            }
                        },
                    },

                    #[name = "gateway_selector"]
                    gtk::ListBox {
                        set_selection_mode: gtk::SelectionMode::Single,
                        add_css_class: "navigation-sidebar",

                        gtk::ListBoxRow {
                            gtk::Label {
                                set_label: &fl!(LOCALE, "gateway"),
                            },
                        },

                        gtk::ListBoxRow {
                            gtk::Label {
                                set_label: &fl!(LOCALE, "new-mapping"),
                            },
                        },

                        connect_row_selected[sender, sidebar_names] => move |this, row| {
                            match row {
                                None => {},
                                Some(r) => {
                                    let gateway = this.row_at_index(0).unwrap();
                                    let new_mapping = this.row_at_index(1).unwrap();
                                    let msg =if *r == gateway {
                                        Message::GoToGateway
                                    }else {
                                        assert!(new_mapping == *r);
                                        Message::GoToNew
                                    };
                                    sidebar_names.unselect_all();
                                    sender.input(msg)
                                }
                            }
                        },

                    },

                    gtk::Separator {
                        set_orientation: gtk::Orientation::Horizontal,
                    },

                    #[local_ref]
                    sidebar_names -> gtk::ListBox {
                        set_selection_mode: gtk::SelectionMode::Single,
                        add_css_class: "navigation-sidebar",

                        connect_row_selected[sender, gateway_selector] => move |_, row| {
                            if let Some(row) = row {
                                sender.input(Message::SelectedMapping(row.index() as _));
                                gateway_selector.unselect_all();
                            }
                        }
                    }

                },

                append = &gtk::Separator {
                    set_orientation: gtk::Orientation::Vertical,
                } -> {
                    set_navigatable: false,
                },

                gtk::Box {
                    set_orientation: gtk::Orientation::Vertical,
                    set_hexpand: true,

                    #[name = "content_header"]
                    adw::HeaderBar {
                        pack_start = &gtk::Button {
                            set_icon_name: "go-previous-symbolic",
                            connect_clicked[leaflet] => move |_| {
                                leaflet.navigate(adw::NavigationDirection::Back);
                            }
                        },

                        #[name = "page_title"]
                        #[wrap(Some)]
                        set_title_widget = &adw::WindowTitle {
                            set_title: &fl!(LOCALE, "gateway"),
                        }
                    },

                    #[name = "toast_overlay"]
                    adw::ToastOverlay {
                        #[name = "stack"]
                        gtk::Stack {}
                    }

                },
            },
        }
    }

    // Initialize the UI.
    fn init(state: State, root: Self::Root, sender: ComponentSender<Self>) -> ComponentParts<Self> {
        let mut mappings_name: FactoryVecDeque<MappingName> = FactoryVecDeque::builder()
            .launch(gtk::ListBox::default())
            .forward(sender.input_sender(), |_| Message::None);

        let (backend, rx) = unbounded_channel();
        // We first collect to a vector to ensure we have the same order when adding them to the stack.
        let mut mapping_widgets = Vec::with_capacity(state.mappings.len());
        for mapping in state.mappings {
            let name_idx = mappings_name.guard().push_back(MappingName {
                uuid: mapping.uuid,
                name: mapping.name.clone(),
                state: mapping::State::Idle,
            });
            let backend = backend.clone();
            mapping_widgets.push((
                mapping.uuid,
                MappingModel::builder()
                    .launch(MappingInit {
                        uuid: mapping.uuid,
                        name: mapping.name,
                        ports: mapping.ports,
                        window: root.clone(),
                    })
                    .forward(
                        sender.input_sender(),
                        mapping_out_handler(backend, mapping.uuid, mapping.ports, name_idx),
                    ),
            ));
        }

        let backend_cl = backend.clone();

        let gateway = Gateway::builder().launch(state.gateway).forward(
            sender.input_sender(),
            move |i| match i {
                GatewayOut::Save => Message::Save,
                GatewayOut::Toast(toast) => Message::AddToast(toast),
                GatewayOut::GatewayChanged(ip) => {
                    backend_cl.send(backend::Message::UseGateway(ip)).ok();
                    Message::None
                }
            },
        );

        let new_mapping = NewMapping::builder()
            .launch(())
            .forward(sender.input_sender(), |msg| match msg {
                NewMappingMessage::NewMapping(mapping) => Message::NewMapping(mapping),
                NewMappingMessage::Toast(toast) => Message::AddToast(toast),
            });

        // Insert the macro code generation here
        let sidebar_names = mappings_name.widget();
        let widgets = view_output!();

        // Add all mappings to the stack
        widgets.stack.add_child(gateway.widget());
        widgets.stack.add_child(new_mapping.widget());
        for (_, m) in &mapping_widgets {
            widgets.stack.add_child(m.widget());
        }

        widgets
            .stack
            .set_transition_type(gtk::StackTransitionType::SlideUpDown);
        widgets
            .gateway_selector
            .select_row(widgets.gateway_selector.row_at_index(0).as_ref());

        let model = AppModel {
            mappings_name,
            new_mapping,
            gateway,
            mapping_widgets: mapping_widgets.into_iter().collect(),
            backend,
            stack: widgets.stack.clone(),
            selector: widgets.gateway_selector.clone(),
            toast_overlay: widgets.toast_overlay.clone(),
            page_title: widgets.page_title.clone(),
            window: widgets.window.clone(),
        };

        let input_sender = sender.input_sender().clone();
        sender.oneshot_command(async move {
            tokio::spawn(run_backend(rx, input_sender, state.gateway));
        });

        ComponentParts { model, widgets }
    }

    fn post_view() {}

    fn pre_view() {
        #[allow(deprecated)]
        widgets.leaflet.navigate(adw::NavigationDirection::Forward);
    }

    fn update(&mut self, msg: Self::Input, sender: ComponentSender<Self>) {
        match msg {
            Message::SelectedMapping(idx) => {
                let uuid = self.mappings_name.guard().get(idx).map(|n| n.uuid).unwrap();
                let page = self.mapping_widgets.get(&uuid).unwrap();
                self.stack.set_visible_child(page.widget());
                self.page_title.set_title(&page.model().name);
            }
            Message::GoToGateway => {
                self.stack.set_visible_child(self.gateway.widget());
                self.page_title.set_title(&fl!(LOCALE, "gateway"));
            }
            Message::GoToNew => {
                self.stack.set_visible_child(self.new_mapping.widget());
                self.page_title.set_title(&fl!(LOCALE, "new-mapping"));
            }
            Message::Gateway(msg) => self.gateway.emit(msg),
            Message::GatewayError(err) => {
                self.toast_overlay.add_toast(adw::Toast::new(&err));
            }
            Message::Save => {
                let to_save = self.save();
                let input_sender = sender.input_sender().clone();
                sender.spawn_command(move |_| {
                    if let Err(err) = to_save.save() {
                        input_sender
                            .send(Message::AddToast(ToastMessage::new(fl!(
                                LOCALE,
                                "failed-to-save-settings",
                                err = err.to_string()
                            ))))
                            .ok();
                    }
                });
            }
            Message::Mapping(uuid, msg) => {
                let Some(mapping) = self.mapping_widgets.get(&uuid) else {
                    tracing::error!("Got message for unknown mapping: {uuid}");
                    return;
                };

                mapping.emit(msg);
            }
            Message::SaveError(err) => {
                tracing::error!("Got error: {err:?}");
                self.toast_overlay.add_toast(adw::Toast::new(&fl!(
                    LOCALE,
                    "failed-to-save-settings",
                    err = err.to_string()
                )));
            }
            Message::NewMapping(mapping) => {
                self.page_title.set_title(&mapping.name);
                let name_idx = self.mappings_name.guard().push_back(MappingName {
                    uuid: mapping.uuid,
                    name: mapping.name.clone(),
                    state: mapping::State::Idle,
                });
                self.mappings_name.widget().select_row(
                    self.mappings_name
                        .widget()
                        .row_at_index(name_idx.current_index() as _)
                        .as_ref(),
                );

                let widget = MappingModel::builder()
                    .launch(MappingInit {
                        uuid: mapping.uuid,
                        name: mapping.name,
                        ports: mapping.ports,
                        window: self.window.clone(),
                    })
                    .forward(
                        sender.input_sender(),
                        mapping_out_handler(
                            self.backend.clone(),
                            mapping.uuid,
                            mapping.ports,
                            name_idx,
                        ),
                    );
                self.stack.add_child(widget.widget());
                self.stack.set_visible_child(widget.widget());
                self.mapping_widgets.insert(mapping.uuid, widget);
                self.selector.unselect_all();
                sender.input(Message::Save);
            }
            Message::DeleteMapping(idx) => {
                self.page_title.set_title(&fl!(LOCALE, "gateway"));
                let idx = idx.current_index();
                let uuid = self.mapping_widgets[idx].model().port.uuid;
                let Some(widget) = self.mapping_widgets.remove(&uuid) else {
                    return;
                };

                self.stack.set_visible_child(self.gateway.widget());
                self.selector
                    .select_row(self.selector.row_at_index(0).as_ref());
                self.stack.remove(widget.widget());
                self.mappings_name.guard().remove(idx);
                sender.input(Message::Save);
            }
            Message::UpdateName(name, idx) => {
                self.mappings_name
                    .send(idx.current_index(), MappingNameInput::ChangeName(name));
            }
            Message::UpdateState(state, idx) => {
                self.mappings_name
                    .send(idx.current_index(), MappingNameInput::ChangeState(state));
            }
            Message::AddToast(toast) => {
                self.toast_overlay.add_toast(toast.build());
            }
            Message::CopyPort(port) => {
                let clipboard = gdk::Display::default().unwrap().clipboard();
                let PublicIp::Some(ip) = self.gateway.model().public_ip else {
                    return;
                };
                let value = format!("{ip}:{port}");
                clipboard.set_text(&value);
                self.toast_overlay
                    .add_toast(adw::Toast::new(&fl!(LOCALE, "copied", value = value)))
            }
            Message::About => {
                #[allow(deprecated)]
                adw::AboutWindow::builder()
                    .application_icon("fr.sgued.ten_forward")
                    .application_name("Ten Forward")
                    .copyright("Copyright 2023 - Sosthène Guédon")
                    .developer_name("Sosthène Guédon")
                    .license_type(gtk::License::Agpl30)
                    .version(env!("CARGO_PKG_VERSION"))
                    .website("https://ten-forward.sgued.fr")
                    .issue_url("https://gitlab.com/sgued/ten-forward/-/issues")
                    .developers(["Sosthène Guédon"].as_slice())
                    .designers(["Sosthène Guédon"].as_slice())
                    .artists(["Sosthène Guédon"].as_slice())
                    .startup_id(APP_ID)
                    .documenters(["Sosthène Guédon"].as_slice())
                    .translator_credits("Sosthène Guédon")
                    .transient_for(&self.window)
                    // .license(include_str!("../LICENSE"))
                    .build()
                    .show();
            }
            Message::None => {}
        };
    }
}
#[derive(RustEmbed)]
#[folder = "i18n"] // path to the compiled localization resources
struct Localizations;

static LOCALE: Lazy<FluentLanguageLoader> = Lazy::new(|| {
    let language_loader: FluentLanguageLoader = fluent_language_loader!();
    let requested_languages = DesktopLanguageRequester::requested_languages();
    let locales = i18n_embed::select(&language_loader, &Localizations, &requested_languages);
    let locale = locales.unwrap_or_else(|_| vec![language_loader.fallback_language().clone()]);
    language_loader.select_languages(&locale);
    language_loader
});

fn main() {
    tracing_subscriber::fmt::init();

    let state = State::load().unwrap_or_default();
    let app = RelmApp::new(APP_ID);
    app.run::<AppModel>(state);
}
